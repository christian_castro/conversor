import React, { Component } from 'react';
import Dolar from '../Imagens/dolar.png'
import Real from '../Imagens/real.png'


class Conversor extends Component {

    state = {reais: "0.18387", dolar: "5.43834", valor: ""}
  
    alterarEstado = (evt) => {
        this.setState({reais: evt.target.value})   
        this.setState({dolares: evt.target.value}) 
        this.setState({valor: evt.target.value})      
    }


    converterDolarReal = () => {
        const numUm = parseFloat(this.state.reais)
        const numDois = parseFloat(this.state.dolar)
        const resultDolarParaReais = (numUm * numDois).toFixed(2)
        
        this.state.valor === "" ? alert('Campo Vazio!') : this.setState({ resultDolarParaReais })
    }

    limpar = () => {
        this.setState({valor: "", resultDolarParaReais: ""})
    }

    render() {
        return (
            <div className="conversorA">
            <div>
                <h2 className="conversor-moedas"> <img src={Dolar} alt={Dolar}/> {this.props.moedaA}  para <img src={Real} alt={Real}/> {this.props.moedaB}</h2>

                <input type="number" value={this.state.valor} onChange={this.alterarEstado} placeholder="Digite o valor" className="conversor-moedas-input"/> <br />

                <button onClick={this.converterDolarReal} className="conversor-moedas-button">Converter</button>

                <button onClick={this.limpar} className="conversor-moedas-button">Limpar</button>
                
                

                <h2 className="result-conversor-sub">Valor Convertido: <span style={{color: 'seashell'}}>R${this.state.resultDolarParaReais}</span></h2>
            </div>
        </div>
        );
    }
}

export default Conversor;