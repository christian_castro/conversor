import React, { Component } from 'react';
import Real from '../Imagens/real.png'
import Dolar from '../Imagens/dolar.png'

class Conversor extends Component {

    state = {reais: "0.18387", dolar: "5.43834", valor: ""}

    alterarEstado = (evt) => {
        this.setState({reais: evt.target.value})   
        this.setState({dolares: evt.target.value}) 
        this.setState({valor: evt.target.value})      
    }


    converterDolarReal = () => {
        const numUm = parseFloat(this.state.reais)
        const numDois = parseFloat(this.state.dolar)
        const resultReaisParaDolar = (numUm / numDois).toFixed(2)
        this.state.valor === "" ? alert('Campo Vazio!') : this.setState({ resultReaisParaDolar })
    }

    limpar = () => {
        this.setState({valor: "", resultReaisParaDolar: ""})
    }

    render() {
        return (
            <div className="conversorB">
                <h2 className="conversor-moedas"> <img src={Real} alt={Real}/> {this.props.moedaA} para <img src={Dolar} alt={Dolar}/> {this.props.moedaB}</h2> 

                <input type="number" value={this.state.valor} onChange={this.alterarEstado}  placeholder="Digite o valor" className="conversor-moedas-input" />

                <br />

                <button onClick={this.converterDolarReal} className="conversor-moedas-button">Converter</button>

                <button onClick={this.limpar} className="conversor-moedas-button">Limpar</button>
                
                <h2>Valor Convertido: <span style={{color: 'seashell'}}>${this.state.resultReaisParaDolar}</span></h2>
            </div>
        );
    }
}

export default Conversor;