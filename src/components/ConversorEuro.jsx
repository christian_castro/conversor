import React, { Component } from 'react';
import Euro from '../Imagens/euro.png'
import Real from '../Imagens/real.png'

class Conversor extends Component {

    constructor(props) {
        super(props);

        this.state = {reais: "0.18387", euro: "6.53419", valor: ""}
    }


    alterarEstado = (evt) => {
        this.setState({reais: evt.target.value})   
        this.setState({euros: evt.target.value}) 
        this.setState({valor: evt.target.value})      
    }


    converterDolarReal = () => {
        const numUm = parseFloat(this.state.reais)
        const numDois = parseFloat(this.state.euro)
        const resultEuroParaReais = (numUm * numDois).toFixed(2)
        this.state.valor === "" ? alert('Campo Vazio!') : this.setState({ resultEuroParaReais })
    }

    limpar = () => {
        this.setState({valor: "", resultEuroParaReais: ""})
    }

    render() {
        return (
            <div className="conversorC">
                <h2 className="conversor-moedas"> <img src={Euro} alt={Euro}/> {this.props.moedaA} para  <img src={Real} alt={Real}/> {this.props.moedaB}</h2> 

                <input type="number" value={this.state.valor} onChange={this.alterarEstado}  placeholder="Digite o valor" className="conversor-moedas-input" />

                <br />

                <button onClick={this.converterDolarReal} className="conversor-moedas-button">Converter</button>

                <button onClick={this.limpar} className="conversor-moedas-button">Limpar</button>
                
                <h2>Valor Convertido: <span style={{color: 'seashell'}}>R${this.state.resultEuroParaReais}</span></h2>
            </div>
        );
    }
}

export default Conversor;