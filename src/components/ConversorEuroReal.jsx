import React, { Component } from 'react';
import Real from '../Imagens/real.png'
import Euro from '../Imagens/euro.png'

class Conversor extends Component {

    constructor(props) {
        super(props);

        this.state = {reais: "0.18387", euro: "6.53419", valor: ""}
    }


    alterarEstado = (evt) => {
        this.setState({reais: evt.target.value})   
        this.setState({euros: evt.target.value}) 
        this.setState({valor: evt.target.value})      
    }


    converterDolarReal = () => {
        const numUm = parseFloat(this.state.reais)
        const numDois = parseFloat(this.state.euro)
        const resultRealEuro = (numUm / numDois).toFixed(2)
        this.state.valor === "" ? alert('Campo Vazio!') : this.setState({ resultRealEuro })
    }

    limpar = () => {
        this.setState({valor: "", resultRealEuro: ""})
    }

    render() {
        return (
            <div className="conversorD">
                <h2 className="conversor-moedas"> <img src={Real} alt={Real}/> {this.props.moedaA} para <img src={Euro} alt={Euro}/> {this.props.moedaB}</h2> 

                <input type="number" value={this.state.valor} onChange={this.alterarEstado}  placeholder="Digite o valor" className="conversor-moedas-input" />

                <br />

                <button onClick={this.converterDolarReal} className="conversor-moedas-button">Converter</button>

                <button onClick={this.limpar} className="conversor-moedas-button">Limpar</button>
                
                <h2>Valor Convertido: <span style={{color: 'seashell'}}>${this.state.resultRealEuro}</span></h2>
            </div>
        );
    }
}

export default Conversor;