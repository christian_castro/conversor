import React, { Component } from 'react';
import Real from '../Imagens/real.png'
import Libra from '../Imagens/libra.png'

class Conversor extends Component {

    state = {reais: "0.18387", libra: "7.506", valor: ""}

    alterarEstado = (evt) => {
        this.setState({reais: evt.target.value})   
        this.setState({libras: evt.target.value}) 
        this.setState({valor: evt.target.value})      
    }


    converterDolarReal = () => {
        const numUm = parseFloat(this.state.reais)
        const numDois = parseFloat(this.state.libra)
        const resultLibraParaReal = (numUm * numDois).toFixed(2)
        this.state.valor === "" ? alert('Campo Vazio!') : this.setState({ resultLibraParaReal })
    }

    limpar = () => {
        this.setState({valor: "", resultLibraParaReal: ""})
    }

    render() {
        return (
            <div className="conversorE">
                <h2 className="conversor-moedas"> <img src={Libra} alt={Libra}/> {this.props.moedaA} para <img src={Real} alt={Real}/> {this.props.moedaB}</h2> 

                <input type="number" value={this.state.valor} onChange={this.alterarEstado}  placeholder="Digite o valor" className="conversor-moedas-input" />

                <br />

                <button onClick={this.converterDolarReal} className="conversor-moedas-button">Converter</button>

                <button onClick={this.limpar} className="conversor-moedas-button">Limpar</button>
                
                <h2>Valor Convertido: <span style={{color: 'seashell'}}>R${this.state.resultLibraParaReal}</span></h2>
            </div>
        );
    }
}

export default Conversor;