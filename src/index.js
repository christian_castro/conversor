import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'

import Conversor from './components/Conversor'
import ConversorDois from './components/ConversorDois'
import ConversorEuro from './components/ConversorEuro'
import ConversorEuroReal from './components/ConversorEuroReal'
import ConversorRealLibra from './components/ConversorRealLibra'
import ConversorLibra from './components/ConversorLibra'

ReactDOM.render(
  <div className="todos-conversores">
    <h1 className="conversor-titulo">Conversor</h1> <br />
  <div className="conversor">
    <Conversor moedaA="Dólar" moedaB="Real" /> 
    <ConversorDois moedaA="Real" moedaB="Dólar"/>
    <ConversorEuro moedaA="Euro" moedaB="Real" />
    <ConversorEuroReal moedaA="Real" moedaB="Euro" />
    <ConversorRealLibra moedaA="Real" moedaB="Libra" />
    <ConversorLibra moedaA="Libra" moedaB="Real" />

  </div>
  <div className="cotacao-center">
    <p className="footer-cotacao">Cotação: 01/05/2021</p>
  </div>
  <footer className="footer">Desenvolvido por &copy; Christian Silveira</footer>
  </div>,
  document.getElementById('root')
);
